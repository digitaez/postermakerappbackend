const express = require("express");
const {
  login,
  getMe,
  logout,
  resetPassword,
  forgotPassword,
  updatePassword,
  updateDetails,
  register,
} = require("../controllers/auth");
const router = express.Router();
const { protect, authorize } = require("../middleware/auth");

router.post("/login", login);
router.get("/logout", logout);
router.get("/me", protect, getMe);
//router.post("/register", register); //protect, authorize("admin"),
router.put("/updatedetails", protect, updateDetails);
//router.put("/updatepassword", protect, updatePassword);
//router.post("/forgotPassword", forgotPassword);
//router.put("/resetPassword/:resetToken", resetPassword);

module.exports = router;
