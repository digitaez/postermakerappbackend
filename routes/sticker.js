const express = require("express");
const advanceResults = require("../middleware/advanceResults");
const Sticker = require("../models/Sticker");
const {
  getStickers,
  deleteSticker,
  addSticker,
} = require("../controllers/sticker");
const router = express.Router();
const { protect, authorize } = require("../middleware/auth");
const BackgroundImage = require("../models/Sticker");

router
  .route("/")
  .post(protect, authorize("admin"), addSticker)
  .get(advanceResults(BackgroundImage), getStickers);

router.route("/:id").delete(protect, authorize("admin"), deleteSticker);

module.exports = router;
