const express = require("express");
const advanceResults = require("../middleware/advanceResults");
const Texture = require("../models/Texture");
const {
  getTextures,
  addTexture,
  deleteTexture,
} = require("../controllers/texture");
const router = express.Router();
const { protect, authorize } = require("../middleware/auth");

router
  .route("/")
  .post(protect, authorize("admin"), addTexture)
  .get(advanceResults(Texture), getTextures);

router.route("/:id").delete(protect, authorize("admin"), deleteTexture);

module.exports = router;
