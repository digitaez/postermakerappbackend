const express = require("express");
const advanceResults = require("../middleware/advanceResults");
const BackgroundImage = require("../models/BackgroundImage");
const {
  getBackgroundImages,
  addBackgroundImage,
  deleteBackgroundImage,
} = require("../controllers/backgroundImage");
const router = express.Router();
const { protect, authorize } = require("../middleware/auth");

router
  .route("/")
  .post(protect, authorize("admin"), addBackgroundImage)
  .get(advanceResults(BackgroundImage), getBackgroundImages);

router.route("/:id").delete(protect, authorize("admin"), deleteBackgroundImage);

module.exports = router;
