const BackgroundImage = require("../models/BackgroundImage");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const fs = require("fs");

// @desc  Get all Background Images
// public - accces to all

exports.getBackgroundImages = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advanceResults);
});

// @desc  add background image
// @route POST /api/v1/backgroundImage
// private - acccess to admin
exports.addBackgroundImage = asyncHandler(async (req, res, next) => {
  console.log(req);
  if (req.files) {
    const file = req.files.file;
    var image = await BackgroundImage.find({
      photo: `backgroundImage_${file.name}`,
    });

    if (image.length != 0) {
      return next(
        new ErrorResponse(`Image alrready present with name ${file.name}`, 401)
      );
    }

    //photo validation
    if (!file.mimetype.startsWith("image")) {
      return next(new ErrorResponse(`Please upload image file`, 400));
    }
    if (file.size > process.env.MAX_FILE_UPLOAD) {
      return next(
        new ErrorResponse(
          `Please upload image less than ${process.env.MAX_FILE_UPLOAD}`,
          400
        )
      );
    }

    // create custom file name
    file.name = `backgroundImage_${file.name}`;

    await file.mv(
      `${process.env.FILE_UPLOAD_PATH}/backgroundImage/${file.name}`
    );

    var backgroundImage = await BackgroundImage.create({ photo: file.name });
    console.log(`photo uploaded ${file.name}`);

    return res.status(200).json({ success: true, data: backgroundImage });
  }

  return next(new ErrorResponse(`Please insert image`, 401));
});

exports.deleteBackgroundImage = asyncHandler(async (req, res, next) => {
  var backgroundImage = await BackgroundImage.findById(req.params.id);
  if (!backgroundImage) {
    return next(
      new ErrorResponse(
        `Background Image not found of id ${req.params.id}`,
        404
      )
    );
  }
  var image = await BackgroundImage.findById(req.params.id).select("photo");
  console.log(image.photo);
  fs.unlinkSync(
    `${process.env.FILE_UPLOAD_PATH}/backgroundImage/${image.photo}`
  );
  await BackgroundImage.findByIdAndDelete(req.params.id);
  res.status(200).json({ success: true, data: {} });
});
