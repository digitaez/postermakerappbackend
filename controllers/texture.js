const Texture = require("../models//Texture");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const fs = require("fs");

// @desc  Get all texture
// public - accces to all

exports.getTextures = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advanceResults);
});

// @desc  add texture
// @route POST /api/v1/texture
// private - acccess to admin
exports.addTexture = asyncHandler(async (req, res, next) => {
  if (req.files) {
    const file = req.files.file;
    var image = await Texture.find({
      photo: `texture_${file.name}`,
    });

    if (image.length != 0) {
      return next(
        new ErrorResponse(`Image already present with name ${file.name}`, 401)
      );
    }

    //photo validation
    if (!file.mimetype.startsWith("image")) {
      return next(new ErrorResponse(`Please upload image file`, 400));
    }
    if (file.size > process.env.MAX_FILE_UPLOAD) {
      return next(
        new ErrorResponse(
          `Please upload image less than ${process.env.MAX_FILE_UPLOAD}`,
          400
        )
      );
    }

    // create custom file name
    file.name = `texture_${file.name}`;

    await file.mv(`${process.env.FILE_UPLOAD_PATH}/texture/${file.name}`);

    var texture = await Texture.create({ photo: file.name });
    console.log(`photo uploaded ${file.name}`);

    return res.status(200).json({ success: true, data: texture });
  }

  return next(new ErrorResponse(`Please insert image`, 401));
});

exports.deleteTexture = asyncHandler(async (req, res, next) => {
  var texture = await Texture.findById(req.params.id);
  if (!texture) {
    return next(
      new ErrorResponse(`Texture not found of id ${req.params.id}`, 404)
    );
  }
  var image = await Texture.findById(req.params.id).select("photo");
  console.log(image.photo);
  fs.unlinkSync(`${process.env.FILE_UPLOAD_PATH}/texture/${image.photo}`);
  await Texture.findByIdAndDelete(req.params.id);
  res.status(200).json({ success: true, data: {} });
});
