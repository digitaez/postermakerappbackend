const User = require("../models/User");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const sendEmail = require("../utils/sendEmail");
const crypto = require("crypto");
const path = require("path");

//desc    Register USer
//route   Get /api/v1/auth/register
//public

// exports.register = asyncHandler(async (req, res, next) => {
//   const { username, email, password } = req.body;

//   var user = await User.create({
//     username,
//     email,
//     password,
//   });
//   user.password = password;
//   user = await user.save();
//   if (req.files) {
//     const file = req.files.file;

//     //photo validation
//     if (!file.mimetype.startsWith("image")) {
//       return next(new ErrorResponse(`Please upload image file`, 400));
//     }
//     if (file.size > process.env.MAX_FILE_UPLOAD) {
//       return next(
//         new ErrorResponse(
//           `Please upload image less than ${process.env.MAX_FILE_UPLOAD}`,
//           400
//         )
//       );
//     }

//     // create custom file name
//     file.name = `photo_${req.user.id}${path.parse(file.name).ext}`;

//     await file.mv(`${process.env.FILE_UPLOAD_PATH}/profile/${user.id}`);

//     req.body.photo = file.name;
//     console.log(`photo uploaded ${file.name}`);
//   }

//   user = await User.findByIdAndUpdate(user.id, req.body, {
//     new: true,
//     runValidators: true,
//   });

//   SendTokenResponse(user, 200, res);
// });

//desc    login User
//route   post /api/v1/auth/login
//public

//postermaker11@gmail.com  posterMaker123
exports.login = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body;

  //validate with email and password
  if (!email || !password) {
    return next(new ErrorResponse("Please provide an email and password", 400));
  }
  //check user
  const user = await User.findOne({ email }).select("+password");

  if (!user) {
    return next(new ErrorResponse("Invalid  Credential", 401));
  }

  const isMatch = await user.matchPassword(password);

  if (!isMatch) {
    return next(new ErrorResponse("Invalid  Credential", 401));
  }
  if (user.status === "Inactive") {
    return next(new ErrorResponse("Status is Inactive", 401));
  }

  SendTokenResponse(user, 200, res);
});

//desc    get current user login
//route   Get /api/v1/auth/me
//private -- need token

exports.getMe = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user.id).select("+password");

  res.status(200).json({ success: true, data: user });
});
//desc    get logout and clear cookie
//route   Get /api/v1/auth/logout
//private -- need token

exports.logout = asyncHandler(async (req, res, next) => {
  res.cookie("token", "none", {
    expires: new Date(Date.now + 10 * 1000),
    httpOnly: true,
  });
  res.status(200).json({ success: true, data: {} });
});

//desc    upfate user name and email detail
//route   PUT /api/v1/auth/updatedetails/:id
//private -- need token
exports.updateDetails = asyncHandler(async (req, res, next) => {
  console.log(req.body);
  var user = await User.findById(req.user.id).select("+password");

  if (!user) {
    return next(new ErrorResponse(`User  not found`, 404));
  }

  if (req.files) {
    const file = req.files.file;

    //photo validation
    if (!file.mimetype.startsWith("image")) {
      return next(new ErrorResponse(`Please upload image file`, 400));
    }
    if (file.size > process.env.MAX_FILE_UPLOAD) {
      return next(
        new ErrorResponse(
          `Please upload image less than ${process.env.MAX_FILE_UPLOAD}`,
          400
        )
      );
    }

    // create custom file name
    file.name = `photo_${req.user.id}${path.parse(file.name).ext}`;

    await file.mv(`${process.env.FILE_UPLOAD_PATH}/profile/${file.name}`);

    req.body.photo = file.name;
    console.log(`photo uploaded ${file.name}`);
  }

  user = await User.findByIdAndUpdate(req.user.id, req.body, {
    new: true,
    runValidators: true,
  });

  if (req.body.password) {
    user.password = req.body.password;
    await user.save();
  }

  res.status(200).json({ success: true, data: user });
});

//get token from model , creatte cookie and send response
const SendTokenResponse = (user, statusCode, res) => {
  const token = user.getSignedJwtToken();

  const options = {
    expires: new Date(
      Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000
    ),
    httpOnly: true,
  };
  if (process.env.NODE_ENV === "production") {
    options.secure = true;
  }
  res
    .status(statusCode)
    .cookie("token", token, options)
    .json({ success: true, token });
};
