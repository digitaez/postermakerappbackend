const Sticker = require("../models/Sticker");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const fs = require("fs");

// @desc  Get all Sticker
// public - accces to all

exports.getStickers = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advanceResults);
});

// @desc  add Sticker
// @route POST /api/v1/sticker
// private - acccess to admin
exports.addSticker = asyncHandler(async (req, res, next) => {
  if (req.files) {
    const file = req.files.file;
    var image = await Sticker.find({
      photo: `sticker_${file.name}`,
    });

    if (image.length != 0) {
      return next(
        new ErrorResponse(`Image already present with name ${file.name}`, 401)
      );
    }

    //photo validation
    if (!file.mimetype.startsWith("image")) {
      return next(new ErrorResponse(`Please upload image file`, 400));
    }
    if (file.size > process.env.MAX_FILE_UPLOAD) {
      return next(
        new ErrorResponse(
          `Please upload image less than ${process.env.MAX_FILE_UPLOAD}`,
          400
        )
      );
    }

    // create custom file name
    file.name = `sticker_${file.name}`;

    await file.mv(`${process.env.FILE_UPLOAD_PATH}/sticker/${file.name}`);

    var sticker = await Sticker.create({ photo: file.name });
    console.log(`photo uploaded ${file.name}`);

    return res.status(200).json({ success: true, data: sticker });
  }

  return next(new ErrorResponse(`Please insert image`, 401));
});

exports.deleteSticker = asyncHandler(async (req, res, next) => {
  var sticker = await Sticker.findById(req.params.id);
  if (!sticker) {
    return next(
      new ErrorResponse(`Sticker not found of id ${req.params.id}`, 404)
    );
  }
  var image = await Sticker.findById(req.params.id).select("photo");
  console.log(image.photo);
  fs.unlinkSync(`${process.env.FILE_UPLOAD_PATH}/sticker/${image.photo}`);
  await Sticker.findByIdAndDelete(req.params.id);
  res.status(200).json({ success: true, data: {} });
});
