const mongoose = require("mongoose");

const StickerSchema = new mongoose.Schema({
  photo: {
    type: String,
    required: [true, "Please attach a image"],
  },

  createAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Sticker", StickerSchema);
