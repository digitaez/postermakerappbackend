const mongoose = require("mongoose");

const BackgroundImageSchema = new mongoose.Schema({
  photo: {
    type: String,
    required: [true, "Please attach a image"],
  },

  createAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Background", BackgroundImageSchema);
