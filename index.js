const express = require("express");
const dotenv = require("dotenv");
const morgan = require("morgan"); // log request and error
const fileupload = require("express-fileupload");
const path = require("path");
const cors = require("cors");
const errorHandler = require("./middleware/error");
const cookieParser = require("cookie-parser");
const connectDB = require(`./config/db`);
const texture = require("./routes/texture");
const auth = require("./routes/auth");
const sticker = require("./routes//sticker");

const backgroundImage = require("./routes/backgroundImage");

dotenv.config({ path: "./config/config.env" });

connectDB();

const app = express();
app.use(cors());
//app.options("*", cors());
//body parser
app.use(express.json());

app.use(cookieParser());

if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

app.use(fileupload());

app.use(express.static(path.join(__dirname, "public")));

app.use("/auth", auth);
app.use("/backgroundImage", backgroundImage);
app.use("/texture", texture);
app.use("/sticker", sticker);

app.get("/", (req, res) => res.send("Welcome to Poster Maker server"));
app.use(errorHandler);

const PORT = process.env.PORT || 3000;
const server = app.listen(
  PORT,
  console.log(`Server is running in ${process.env.NODE_ENV} mode on ${PORT}`)
);

process.on("unhandledRejection", (err, promise) => {
  console.log(`Error : ${err.message}`);
  //close server & exit process
  server.close(() => process.exit(1));
});
